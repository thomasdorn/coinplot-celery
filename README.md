# Celery task server running inside a docker container

# Installation
```sh
$ git clone https://gitlab.com/thomasdorn/coinplot-celery.git
$ cd coinplot-celery
$ sh start.sh
```

- You can change -j8 in the entrypoint where 8 would be the number of threads your processor has.