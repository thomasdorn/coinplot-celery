#!/bin/bash

pythonversion="3.6.5"

apt-get -y update \
    &&  service rabbitmq-server start \
    &&  rabbitmqctl add_user celeryuser celery \
    &&  rabbitmqctl add_vhost celeryvhost \
    &&  rabbitmqctl set_permissions -p celeryvhost celeryuser ".*" ".*" ".*" \
    &&  service rabbitmq-server restart \
    &&  apt-get -y upgrade \
    &&  apt-get install -y make build-essential libssl-dev zlib1g-dev \
    &&  apt-get install -y libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm \
    &&  apt-get install -y libncurses5-dev  libncursesw5-dev xz-utils tk-dev \
    &&  wget https://www.python.org/ftp/python/${pythonversion}/Python-${pythonversion}.tgz \
    &&  tar xvf Python-${pythonversion}.tgz \
    &&  mv Python-${pythonversion} python \
    &&  cd python \
    &&  ./configure \
    &&  make -j8 \
    &&  make altinstall \
    &&  ./python -m pip install -r ../requirements.txt \
    &&  cd .. \
    &&  export PATH="$PATH:/workspace" \
    &&  export LC_ALL=C.UTF-8 \
    &&  export LANG=C.UTF-8 \
    &&  pm2 start /redis-stable/src/redis-server \
    &&  pm2 start celery.sh \
    &&  pm2 start proc0.sh \
    &&  pm2 logs