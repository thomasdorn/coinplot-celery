import time
from tasks import runFactorial, runSimulation

def main():
    runFactorial.delay(50)
    runSimulation.delay(1000)


if __name__ == "__main__":
    while(True):
        time.sleep(5)
        main()