import numpy as np
from celery import Celery

queue_handle = Celery( 'tasks', broker = 'amqp://celeryuser:celery@localhost:5672/celeryvhost', backend = 'redis://127.0.0.1')

def factorial(n):
    if n == 1:
        return 1
    else:
        res = n * factorial(n-1)
        print("intermediate result for ", n, " * factorial(" ,n-1, "): ",res)
        return res


@queue_handle.task
def runFactorial(n):
    try :
        print(factorial(n))
    except RecursionError :
        pass ## use to run example again



@queue_handle.task
def runSimulation(n):
    N = n
    dt = 0.1
    x = np.zeros(N)
    v = np.zeros(N)
    t = np.arange(0, (N + 0.5) * dt, dt)
    a = np.ones(N) * 1.0  # initial condition
    x[[0, 1]] = 1
    v[1] = v[0] + a[0] * dt

    for i in range(1, N - 1):
        x[i + 1] = x[i] + v[i] * dt + (a[i] * (dt ** 2) * 0.5)
        v[i + 1] = v[i] + a[i] * dt

    print("Final Position: " + str(x))
    print("Final Velocity: " + str(v))

